.. (C) ALbert Mietus, Sogeti, 2019

==============
Measure & Blog
==============
:author: Albert
:date: 2019

The DDS-Demonstrators use the ‘Measure & Blog’ approach: every project and every iteration starts with an (R&D)
question, and ends with a publication on the answer.
|BR|
Typical, the project included some ‘measurements’, and the publication is a simple ‘blog’.

The intermediate results, like code, automatic test- and measure-scripts, and other documentation are also *published*:
should be stored in this version control system. In such a way that others can repeat the experiment easily and learn quickly.

Often, each ‘Measure & Blog’ will lead to new questions. They should also be documented and can lead to new iterations.

More on this, later in a generic blog ...

Deliverables
============

Every iteration [#iteration]_ sprint gives the following deliveries:

#) The *production code* (read: a DDS-Demonstrator, in functional, partial deliveries).
#) The design, where relevant: Use plantUML diagrams, text, etc.
#) The test- and measurement-code.
#) User manuals (SUM: Software User Manuals) - of the product (how to install, run, use ...).
#) Project / Sprint documentation (including estimates, comments).
#) The "Blog": from research question by the measurements/results towards the conclusions/advice; typically short.
#) All kind of “convenient notes”; in the :ref:`TeamPages`
#) ‘Free’ documentation improvements on existing (used) documentation *--without adding cost*.
#) All other short, cheap improvements to record knowledge.


.. rubric:: footnotes

.. [#iteration] The term iteration is used to generalize a (scrum) **sprint**. The same deliverables are expected at the
                end of a *super-sprint* (in safe: ‘PI’), an internship, an innovation-assignment and the end of a
                project.

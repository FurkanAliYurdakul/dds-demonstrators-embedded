Things to do:
=============

DDS-Demonstrators
-----------------

Many Demonstrators can/do exist in several variants. And/or measurements between the variants are useful.
Typically there is a backlog of Demonstrators and Requirements (on e.g. variants)

* See the :ref:`DemonstratorList`, for some ideas
* And the ‘incoming’ liks there for variants or requirements

TodoList
--------

There are also a lot of things on the todolist; collected from the docs


.. todolist::

Ask ...
--------

For all assignments, ask the PO (albert)

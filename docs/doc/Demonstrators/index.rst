.. (C) ALbert Mietus, Sogeti, 2019

.. _DemonstratorList:

*********************
List of Demonstrators
*********************

Each Demonstrator will be documented in it own directory; by the developer(s). See below for the list of DDS-Demonstrator
(in various stages).

.. needtable::
   :types: demo
   :style: table
   :columns: title;id;status;outgoing;incoming;tags


.. note:: for developers

   Please create your own directory (within your Feature-branch/fork) with a name as set by the product-owner. Both in
   the :file:`src` and :file:`docs/doc/Demonstrators` directory. Use the doc-directory to document the Demonstrator.

   See the :ref:`TeamPages` directory for “scratch” documentation on personal/team-level. Also, create a subdir there.

The (toplevel) requirements and backlog of Demonstrator can be found: :ref:`backlog`

.. toctree::
   :maxdepth: 2
   :glob:

   */index



